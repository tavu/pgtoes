package es

import (
	"context"
	"fmt"

	"github.com/elastic/go-elasticsearch/esapi"
	es7 "github.com/elastic/go-elasticsearch/v7"
)

// DropIndicesIfExists deletes given indices if they exists
func DropIndicesIfExists(ctx context.Context, es *es7.Client, index ...string) error {
	var indices []string
	indices = append(indices, index...)
	req := esapi.IndicesDeleteRequest{Index: indices}
	res, err := req.Do(context.Background(), es)
	if err != nil {
		return fmt.Errorf("Error getting response: %s", err)
	}
	_ = res.Body.Close()
	return nil
}
