package es_test

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/Jeffail/gabs/v2"
	es7 "github.com/elastic/go-elasticsearch/v7"
	"gitlab.com/tavu/pgtoes/es"
	"gotest.tools/assert"
)

var (
	esClient *es7.Client
)

func getESClient() *es7.Client {
	if esClient == nil {
		cfg := es7.Config{
			Addresses: []string{
				os.Getenv("INDEX_TARGET"),
			},
		}
		client, err := es7.NewClient(cfg)
		if err != nil {
			log.Fatalln(err)
		}
		esClient = client
	}
	return esClient
}

func TestDropIndices(t *testing.T) {
	indexName := "testpgtoes"
	indexSeparator := "-"
	client := getESClient()

	// index document
	documentID1 := "1"
	{
		blob := gabs.New()
		_, _ = blob.Set(1, "myid")
		_, _ = blob.Set("bar", "myfoo")
		_, _ = client.Index(
			indexName,
			strings.NewReader(blob.String()),
			client.Index.WithDocumentID(documentID1),
		)
	}

	documentID2 := "2"
	documentID2Index := fmt.Sprintf("%s%s2006.01.01", indexName, indexSeparator)
	{
		blob := gabs.New()
		_, _ = blob.Set(2, "myid")
		_, _ = blob.Set("bar2", "myfoo")
		_, _ = client.Index(
			documentID2Index,
			strings.NewReader(blob.String()),
			client.Index.WithDocumentID(documentID2),
		)
	}

	// document is found
	{
		res, err := client.Get(indexName, documentID1)
		if err != nil {
			t.Error(err)
		}
		defer res.Body.Close()

		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		}

		if res.IsError() {
			log.Fatalf("Error: %s", res.String())
		}
		source := r["_source"].(map[string]interface{})
		assert.Equal(t, "bar", source["myfoo"].(string), "myfoo is bar")
		assert.Equal(t, 1, int(source["myid"].(float64)), "myid is 1")
	}

	{
		res, _ := client.Get(documentID2Index, documentID2)
		defer res.Body.Close()

		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		}

		if res.IsError() {
			log.Fatalf("Error: %s", res.String())
		}
		source := r["_source"].(map[string]interface{})
		assert.Equal(t, "bar2", source["myfoo"].(string), "myfoo is bar")
		assert.Equal(t, 2, int(source["myid"].(float64)), "myid is 1")
	}

	// delete indices
	err := es.DropIndicesIfExists(
		context.Background(),
		client,
		indexName,
		fmt.Sprintf("%s%s*", indexName, indexSeparator),
	)
	if err != nil {
		log.Fatalln(err)
	}

	// document is not found
	{
		res, _ := client.Get(indexName, documentID1)
		defer res.Body.Close()
		assert.Equal(t, 404, res.StatusCode, "status is 404")
	}

	{
		res, _ := client.Get(documentID2Index, documentID2)
		defer res.Body.Close()
		assert.Equal(t, 404, res.StatusCode, "status is 404")
	}

}
