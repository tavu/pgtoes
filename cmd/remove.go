package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/tavu/pgtoes/app"
)

// removeCmd represents the remove command
var removeCmd = &cobra.Command{
	Use:   "remove",
	Short: "Remove created publication and replication slots",
	Long:  `Remove created publication and replication slots`,
	Run: func(cmd *cobra.Command, args []string) {
		app.Get().Remove()
	},
}

func init() {
	rootCmd.AddCommand(removeCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// removeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// removeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
