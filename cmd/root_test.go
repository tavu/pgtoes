package cmd

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tavu/pgtoes/app"
)

func TestParamConfigFile(t *testing.T) {
	// Unset env variables so we get pristine test
	os.Unsetenv("INDEX_SOURCE")
	os.Unsetenv("INDEX_TARGET")

	tmpFile, err := ioutil.TempFile(os.TempDir(), "pgtoes-test-*.yaml")
	if err != nil {
		log.Fatal("Cannot create temporary file", err)
	}
	defer os.Remove(tmpFile.Name())
	yaml := []byte(`---
index:
  name: web
  format: false
  separator: "-xxx-"
  dateformat: "Jan-02-06"
  for: document
  with: insert
  column: id, created, date, title
  source: postgres://mypguser:mypgsecret@xxx:6000/web
  target: http://yyy:9200/

log:
  format: json
  output: stdout

metric:
  enabled: false
  port: 3333
`)
	if _, err = tmpFile.Write(yaml); err != nil {
		log.Fatal("Failed to write to temporary file", err)
	}
	if err := tmpFile.Close(); err != nil {
		log.Fatal(err)
	}
	args := []string{"--config", tmpFile.Name(), "help"}
	rootCmd.SetArgs(args)
	_ = rootCmd.Execute()
	myapp := app.Get()
	cfg := myapp.Config

	// index
	assert.Equal(t, "web", cfg.IndexName, "index.name read from config")
	assert.Equal(t, false, cfg.IndexFormat, "index.format read from config")
	assert.Equal(t, "-xxx-", cfg.IndexSeparator, "index.separator read from config")
	assert.Equal(t, "Jan-02-06", cfg.IndexDateFormat, "index.dateformat read from config")
	assert.Equal(t, "document", cfg.IndexFor[0], "index.for read from config")
	assert.Equal(t, "insert", cfg.IndexWith[0], "index.for read from config")
	assert.Equal(t, "id, created, date, title", cfg.IndexColumn, "index.column read from config")
	assert.Equal(t, "postgres://mypguser:mypgsecret@xxx:6000/web", cfg.IndexSource, "index.source read from config")
	assert.Equal(t, "http://yyy:9200/", cfg.IndexTarget, "index.target read from config")

	// log
	assert.Equal(t, "json", cfg.LogFormat, "log.format read from config")
	assert.Equal(t, "stdout", cfg.LogOutput, "log.type read from config")

	// metric
	assert.Equal(t, false, cfg.MetricEnabled, "metric.enabled read from config")
	assert.Equal(t, 3333, cfg.MetricPort, "metric.port read from config")
}
