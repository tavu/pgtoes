FROM golang:1.15.2 AS builder
RUN mkdir /src
WORKDIR /src
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o pgtoes

FROM debian:buster-slim AS debug
RUN apt-get update -qq
RUN apt-get install -qq netcat iproute2 netcat iputils-ping procps postgresql-client
COPY --from=builder /src/pgtoes /bin/pgtoes

CMD /bin/pgtoes run
