module gitlab.com/tavu/pgtoes

go 1.14

require (
	github.com/Jeffail/gabs/v2 v2.5.0
	github.com/cheggaaa/pb/v3 v3.0.4
	github.com/dustin/go-humanize v1.0.0
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v7 v7.6.0
	github.com/jackc/pgconn v1.5.0
	github.com/jackc/pglogrepl v0.0.0-20200309144228-32ec418076b3
	github.com/jackc/pgproto3/v2 v2.0.1
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	google.golang.org/appengine v1.6.1
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gotest.tools v2.2.0+incompatible
)
