project = "pgtoes"

# Labels can be specified for organizational purposes.
# labels = { "foo" = "bar" }

app "run" {
    
    build {
        use "docker" {}
        
        registry {
          use "docker" {
            image = "registry.gitlab.viidakko.fi/tojo/pgtoes"
            tag   = "latest"
          }
        }

    }

    deploy {
        use "kubernetes" {
            image_secret = "regcred"
        }
    }

}
