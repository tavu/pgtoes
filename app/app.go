package app

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	es7 "github.com/elastic/go-elasticsearch/v7"
	esapi "github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/jackc/pgconn"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tavu/pgtoes/es"
	"gitlab.com/tavu/pgtoes/pg"
)

// App struct
type App struct {
	Config    Config
	PgConn    *pgconn.PgConn
	ReplConn  *pgconn.PgConn
	Es        *es7.Client
	Processor []*pg.FnProcessData
}

// Runtime holds the runtime information
type Runtime struct {
	SlotName        string `json:"slotName"`
	ConsistentPoint string `json:"consistentPoint"`
	SnapshotName    string `json:"snapshotName,omitempty"`
}

var (
	app   *App
	state Runtime
)

const errStr string = "ERROR: %s\n"

// Get application
func Get() *App {
	if app == nil {
		app = &App{}
		app.Config = GetConfig()
		var fn pg.FnProcessData = func(lsn string, blob *pg.WALBlob) {
			state.ConsistentPoint = lsn
			log.Debugf("app.UpdateCurrentState(%+v)", state)
			app.UpdateCurrentState(state)
			log.Debugf("app.ApplyChanges(%+v)", blob)
			app.ApplyChanges(blob)
		}
		app.Processor = append(app.Processor, &fn)
		SetLogger(app.Config)
	}
	return app
}

// Init application
func (app *App) Init() {
	defer func() {
		if err := app.GetReplConn().Close(context.Background()); err != nil {
			fmt.Printf("Error closing DB conn: %s\n", err)
		}
	}()
	fmt.Print("Create replication slot on Postgres database.. ")
	var r Runtime
	{
		slotResult, err := pg.CreateReplicationSlotIfNotExists(
			context.Background(),
			app.GetReplConn(),
			app.GetSlotName(),
		)
		if err != nil {
			fmt.Printf("\n%s\n", err)
			os.Exit(1)
		}
		r = Runtime{
			SlotName:        slotResult.SlotName,
			ConsistentPoint: slotResult.ConsistentPoint,
			SnapshotName:    slotResult.SnapshotName,
		}
		fmt.Println("")
		fmt.Printf(" - Slot name        : %s\n", r.SlotName)
		fmt.Printf(" - Consistent point : %s\n", r.ConsistentPoint)
		fmt.Printf(" - Snapshot name    : %s\n", r.SnapshotName)
	}
	fmt.Println("Done.")

	fmt.Print("Saving current state on Elasticsearch.. ")
	{
		app.UpdateCurrentState(r)
	}
	fmt.Println("Done.")

	fmt.Print("Preparing to import snapshot data to Elasticsearch.. ")
	if r.SnapshotName != "" {
		fmt.Println("")
		app.IndexSnapshotData(r.SnapshotName)
		fmt.Println("Done.")
	} else {
		fmt.Println("Skipped. Snapshot missing")
	}
}

// Run start streaming changes to elasticsearch
func (app *App) Run() {
	state = app.GetCurrentState()
	var opts []string
	opts = append(opts, fmt.Sprintf("\"%s\" '%s'", "actions", strings.Join(app.Config.IndexWith, ",")))
	opts = append(opts, fmt.Sprintf("\"%s\" '%s'", "add-tables", strings.Join(app.Config.IndexFor, ",")))
	err := pg.StartReplication(
		context.Background(),
		app.GetReplConn(),
		app.GetSlotName(),
		state.ConsistentPoint,
		&app.Processor,
		opts,
	)
	if err != nil {
		fmt.Printf(errStr, err)
		os.Exit(-1)
	}
}

// Remove created publication and replication slots
func (app *App) Remove() {

	fmt.Print("Shutting down replication on Postgres database... ")
	{
		err := pg.DropReplicationSlotIfExists(context.Background(), app.GetReplConn(), app.GetSlotName())
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}
	fmt.Println("Done.")

	/*
		fmt.Print("Removing publication on Postgres database... ")
		{
			err := pg.DropPublicationIfExists(context.Background(), app.GetReplConn(), app.GetSlotName())
			if err != nil {
				fmt.Printf(errStr, err)
			}
		}
		fmt.Println("Done.")
	*/

	fmt.Print("Dropping indices on Elasticsearch... ")
	{
		err := es.DropIndicesIfExists(
			context.Background(),
			app.GetEsClient(),
			app.Config.IndexName,
			fmt.Sprintf("%s%s*", app.Config.IndexName, app.Config.IndexSeparator),
		)
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}
	fmt.Println("Done.")
}

// GetReplConn get datatabase connection for replication
func (app *App) GetReplConn() *pgconn.PgConn {
	if app.ReplConn == nil || app.ReplConn.IsClosed() {
		connString := app.Config.IndexSource
		connString += "?replication=database"
		conn, err := pgconn.Connect(context.Background(), connString)
		if err != nil {
			log.Fatalf("Failed to connect to PostgreSQL server: %s", err)
		}
		app.ReplConn = conn
	}
	return app.ReplConn
}

// GetPgConn get datatabase connection for transactions
func (app *App) GetPgConn() *pgconn.PgConn {
	if app.PgConn == nil || app.PgConn.IsClosed() {
		conn, err := pgconn.Connect(context.Background(), app.Config.IndexSource)
		if err != nil {
			log.Fatalln(err)
		}
		app.PgConn = conn
	}
	return app.PgConn
}

// GetEsClient returns Elasticsearch client
func (app *App) GetEsClient() *es7.Client {
	if app.Es == nil {
		cfg := es7.Config{}
		if len(app.Config.IndexTargetCloudID) > 0 {
			cfg.CloudID = app.Config.IndexTargetCloudID
			cfg.Username = app.Config.IndexTargetUsername
			cfg.Password = app.Config.IndexTargetPassword
		} else {
			var target string
			if len(app.Config.IndexTargetURL) > 0 {
				target = app.Config.IndexTargetURL
			} else {
				target = app.Config.IndexTarget
			}
			cfg.Addresses = []string{target}
		}

		client, err := es7.NewClient(cfg)
		if err != nil {
			log.Fatalln(err)
		}
		app.Es = client
	}
	return app.Es
}

// ApplyChanges takes WAL changes as a blob and processes each of those
func (app *App) ApplyChanges(blob *pg.WALBlob) {
	es := app.GetEsClient()
	for _, row := range blob.Change {
		var (
			res        *esapi.Response
			err        error
			documentID string
		)
		indexName := app.GetIndexName(row.Table, "")
		if row.Kind == "insert" || row.Kind == "update" {
			documentID = row.GetID()
			res, err = es.Index(
				indexName,
				strings.NewReader(row.ToJSON()),
				es.Index.WithDocumentID(documentID),
			)
		} else if row.Kind == "delete" {
			documentID = row.GetOldID()
			res, err = es.Delete(
				indexName,
				documentID,
			)
		}
		if err == nil {
			log.WithFields(log.Fields{
				"index": indexName,
				"id":    documentID,
				"kind":  row.Kind,
			}).Info("Document update")
		} else {
			log.Fatalf("Error on document %s: %s", row.Kind, err)
		}
		err = res.Body.Close()
		if err != nil {
			log.Errorf("Error on es.body.close: %s", err)
		}
	}
}

// UpdateCurrentState ...
func (app *App) UpdateCurrentState(runtime Runtime) {
	b, err := json.Marshal(runtime)
	if err != nil {
		log.Println(err)
	}
	es := app.GetEsClient()
	res, err := es.Index(
		app.Config.IndexName,
		strings.NewReader(fmt.Sprintf("%s", b)),
		es.Index.WithDocumentID("runtime"),
		es.Index.WithRefresh("true"),
	)
	if err != nil {
		log.Fatalf("Error on saving state: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Fatalf("Error on saving state: %s", res.String())
	}
}

// GetCurrentState retrieves saved state from Elasticsearch
func (app *App) GetCurrentState() Runtime {
	es := app.GetEsClient()
	res, err := es.Get(app.Config.IndexName, "runtime")
	if err != nil {
		log.Fatalln(err)
	}
	defer res.Body.Close()

	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}

	if res.IsError() {
		log.Fatalf("Error: %s", res.String())
	}
	source := r["_source"].(map[string]interface{})
	if _, ok := source["snapshotName"]; !ok {
		source["snapshotName"] = ""
	}
	state := Runtime{
		SlotName:        source["slotName"].(string),
		ConsistentPoint: source["consistentPoint"].(string),
		SnapshotName:    source["snapshotName"].(string),
	}
	return state
}

// GetIndexName ...
func (app *App) GetIndexName(table, datetime string) string {
	table = strings.Replace(table, "public.", "", 1)
	if app.Config.IndexFormat {
		var idx []string
		idx = append(idx, app.Config.IndexName)
		idx = append(idx, table)
		if len(app.Config.IndexDateFormat) > 0 {
			var (
				t   time.Time
				err error
			)
			if datetime == "" {
				t = time.Now()
			} else {
				layout := "2006-01-02 15:04:05+00"
				t, err = time.Parse(layout, datetime)
				if err != nil {
					t = time.Now()
				}
			}
			idx = append(idx, t.Format(app.Config.IndexDateFormat))
		}
		return strings.Join(idx, app.Config.IndexSeparator)
	}
	return app.Config.IndexName
}

// GetSlotName ...
func (app *App) GetSlotName() string {
	slotName := app.Config.IndexName
	slotName = strings.ReplaceAll(slotName, ".", "_")
	slotName = strings.ReplaceAll(slotName, "-", "_")
	return slotName
}
