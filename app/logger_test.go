package app_test

import (
	_ "fmt"
	"testing"

	log "github.com/sirupsen/logrus"
	"gitlab.com/tavu/pgtoes/app"
	"gotest.tools/assert"
)

func TestSetLogger(t *testing.T) {
	var cfg app.Config
	cfg.LogFormat = "json"
	cfg.LogLevel = "debug"
	cfg.LogOutput = "stdout"
	app.SetLogger(cfg)
	assert.Equal(t, "debug", log.GetLevel().String(), "error level debug")

	cfg.LogLevel = "warn"
	app.SetLogger(cfg)
	assert.Equal(t, "warning", log.GetLevel().String(), "error level warning")

	cfg.LogLevel = "error"
	app.SetLogger(cfg)
	assert.Equal(t, "error", log.GetLevel().String(), "error level error")

	cfg.LogLevel = "fatal"
	app.SetLogger(cfg)
	assert.Equal(t, "fatal", log.GetLevel().String(), "error level fatal")

	defer func() { log.StandardLogger().ExitFunc = nil }()
	var fatal bool
	log.StandardLogger().ExitFunc = func(int) { fatal = true }
	cfg.LogLevel = "typolevel"
	app.SetLogger(cfg)
	assert.Assert(t, fatal, "typoed loglevel and got fatal")
	cfg.LogLevel = "debug"

	fatal = false
	cfg.LogOutput = "typooutput"
	app.SetLogger(cfg)
	assert.Assert(t, fatal, "typoed logoutput and got fatal")
	cfg.LogOutput = "stdout"

	fatal = false
	cfg.LogFormat = "typoformat"
	app.SetLogger(cfg)
	assert.Assert(t, fatal, "typoed logformat and got fatal")
	cfg.LogFormat = "text"
}
