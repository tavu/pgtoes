package app

import (
	"os"

	log "github.com/sirupsen/logrus"
)

// SetLogger configurates application logging
func SetLogger(cfg Config) {
	switch cfg.LogFormat {
	case "text":
		log.SetFormatter(&log.TextFormatter{})
	case "json":
		log.SetFormatter(&log.JSONFormatter{})
	default:
		log.Fatalf("Invalid value for LOG_FORMAT %q", cfg.LogFormat)
	}

	switch cfg.LogOutput {
	case "stdout":
		log.SetOutput(os.Stdout)
	default:
		log.Fatalf("Invalid value for LOG_OUTPUT %q", cfg.LogOutput)
	}

	switch cfg.LogLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.FatalLevel)
	default:
		log.Fatalf("Invalid value for LOG_LEVEL %q", cfg.LogLevel)
	}
}
