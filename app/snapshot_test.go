package app

import (
	"strings"
	"testing"

	"github.com/Jeffail/gabs/v2"
	"gotest.tools/assert"
)

func TestCountStatsOnItems(t *testing.T) {
	var (
		indexed, errors int
		items           []esBulkResponseItem
	)
	items = append(items, esBulkResponseItem{
		Index: esBulkResponseItemIndex{
			ID:     "foo",
			Result: "bar",
			Status: 201,
			Error: esBulkResponseItemIndexError{
				Type:   "fail",
				Reason: "reason",
				Cause: esBulkResponseItemIndexErrorCause{
					Type:   "mytype",
					Reason: "myreason",
				},
			},
		},
	})
	indexed, errors = countStatsOnItems(items, indexed, errors)
	assert.Equal(t, 1, indexed, "indexed count is 1")
	assert.Equal(t, 0, errors, "errors count is 0")
}

func TestSetJSONData(t *testing.T) {
	var vals [][]byte
	vals = append(vals, []byte("yksi"))
	vals = append(vals, []byte("kaksi"))
	dataObj := gabs.New()
	setJSONData(dataObj, strings.Split("foo, bar", ","), vals)
	//json := dataObj.String()
	//assert.Equal(t, `{"bar":"kaksi","foo":"yksi"}`, json, "json data valid")
	assert.Equal(t, "yksi", dataObj.Path("foo").Data().(string), "foo is yksi")
	assert.Equal(t, "kaksi", dataObj.Path("bar").Data().(string), "foo is yksi")
}
