package app_test

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/tavu/pgtoes/pg"

	"github.com/jackc/pgconn"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "gitlab.com/tavu/pgtoes/app"
)

const errStr = "ERROR: %s\n"

var _ = Describe("App", func() {
	var state Runtime
	app := Get()
	{
		app.Remove()
		app.ReplConn = nil
	}
	conn := app.GetPgConn()
	{
		_, err := conn.Exec(context.Background(), "delete from t;").ReadAll()
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}
	{
		_, err := conn.Exec(context.Background(), "insert into t (id, name) values (2000, 'catamount');insert into t (id, name) values (3000, 'lynx');").ReadAll()
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}
	Context("with default configuration", func() {
		Context("when app is first time initialized", func() {
			app.Init()
			app.ReplConn = nil
			conn := app.GetPgConn()
			It("should index snapshot data from database table", func() {
				results, err := conn.Exec(context.Background(), "SELECT name FROM t ORDER BY id ASC;").ReadAll()
				if err != nil {
					fmt.Printf(errStr, err)
				}
				Expect(string(results[0].Rows[0][0])).To(Equal("catamount"))
				Expect(string(results[0].Rows[1][0])).To(Equal("lynx"))

				es := app.GetEsClient()
				indexName := app.GetIndexName("public.t", "")
				res, err := es.Get(indexName, "2000")
				if err != nil {
					fmt.Println(err)
				}
				defer res.Body.Close()

				var r map[string]interface{}
				if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
					fmt.Printf("Error parsing the response body: %s", err)
				}

				doc := make(map[string]interface{})
				if res.IsError() {
					fmt.Printf("Error: %s", res.String())
					doc["id"] = ""
					doc["name"] = ""
				} else {
					doc = r["_source"].(map[string]interface{})
				}
				Expect(int(doc["id"].(float64))).To(Equal(2000))
				Expect(doc["name"].(string)).To(Equal("catamount"))
			})
		})

		Context("when rows changes on database", func() {
			c := make(chan string)
			app.ReplConn = nil
			app.PgConn = nil
			go func() {
				app.Run()
			}()
			go func() {
				var fn pg.FnProcessData = func(_ string, change *pg.WALBlob) {
					c <- change.Change[0].Kind
				}
				app.Processor = append(app.Processor, &fn)
				state = app.GetCurrentState()
				conn, err := pgconn.Connect(context.Background(), app.Config.IndexSource)
				if err != nil {
					fmt.Printf(errStr, err)
					os.Exit(-1)
				}
				_, err = conn.Exec(context.Background(), "insert into t (id, name) values (1000, 'cat');").ReadAll()
				if err != nil {
					fmt.Printf(errStr, err)
				}
			}()
			It("should update elasticsearch indexes", func() {

				By("Update current state")

				Expect(<-c).To(ContainSubstring("insert"))
				updatedState := app.GetCurrentState()
				Expect(state.ConsistentPoint).ShouldNot(BeEquivalentTo(updatedState.ConsistentPoint))

				By("Inserted document is found on index")

				es := app.GetEsClient()
				indexName := app.GetIndexName("t", "")
				res, err := es.Get(indexName, "1000")
				if err != nil {
					fmt.Println(err)
				}
				defer res.Body.Close()

				var r map[string]interface{}
				if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
					fmt.Printf("Error parsing the response body: %s", err)
				}

				doc := make(map[string]interface{})
				if res.IsError() {
					fmt.Printf("Error: %s", res.String())
					doc["id"] = ""
					doc["name"] = ""
				} else {
					doc = r["_source"].(map[string]interface{})
				}
				Expect(int(doc["id"].(float64))).To(Equal(1000))
				Expect(doc["name"].(string)).To(Equal("cat"))

				By("Deleted document is not found on index")

				conn, err := pgconn.Connect(context.Background(), app.Config.IndexSource)
				if err != nil {
					fmt.Printf(errStr, err)
					os.Exit(-1)
				}
				_, err = conn.Exec(context.Background(), "delete from t where id = 1000;").ReadAll()
				if err != nil {
					fmt.Printf(errStr, err)
				}

				Expect(<-c).To(ContainSubstring("delete"))

				res, err = es.Get(indexName, "1000")
				if err != nil {
					fmt.Println(err)
				}
				defer res.Body.Close()

				if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
					fmt.Printf("Error parsing the response body: %s", err)
				}

				Expect(res.IsError()).To(BeTrue())
				Expect(res.StatusCode).To(Equal(404))
			})
		})
	})
})

var _ = Describe("App", func() {
	Context("with custom configuration", func() {
		Context("when index formatting is disabled", func() {
			app := App{}
			app.Config.IndexFormat = false
			app.Config.IndexName = "my-index-name"
			It("should return defined index.name", func() {
				Expect(app.GetIndexName("t", "")).To(Equal(app.Config.IndexName))
			})
		})

	})
})
