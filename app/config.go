package app

import "github.com/spf13/viper"

// Config struct
type Config struct {
	IndexName           string
	IndexFormat         bool
	IndexSeparator      string
	IndexDateFormat     string
	IndexFor            []string
	IndexWith           []string
	IndexColumn         string
	IndexSource         string
	IndexTarget         string
	IndexTargetURL      string
	IndexTargetCloudID  string
	IndexTargetUsername string
	IndexTargetPassword string
	LogFormat           string
	LogOutput           string
	LogLevel            string
	MetricEnabled       bool
	MetricPort          int
}

const (
	indexName           string = "index.name"
	indexFormat         string = "index.format"
	indexSeparator      string = "index.separator"
	indexDateformat     string = "index.dateformat"
	indexFor            string = "index.for"
	indexWith           string = "index.with"
	indexColumn         string = "index.column"
	indexSource         string = "index.source"
	indexTarget         string = "index.target"
	indexTargetURL      string = "index.target.url"
	indexTargetCloudID  string = "index.target.cloudid"
	indexTargetUsername string = "index.target.username"
	indexTargetPassword string = "index.target.password"
	logFormat           string = "log.format"
	logOutput           string = "log.output"
	logLevel            string = "log.level"
	metricEnabled       string = "metric.enabled"
	metricPort          string = "metric.port"
)

// GetConfig get application configuration in order: env, config, cli, defaults
func GetConfig() Config {

	// Set default values to keys
	viper.SetDefault(indexName, "pgtoes")
	viper.SetDefault(indexFormat, true)
	viper.SetDefault(indexSeparator, "-")
	viper.SetDefault(indexDateformat, "2006.01")
	viper.SetDefault(indexFor, []string{"public.t"})
	viper.SetDefault(indexWith, []string{"insert", "update", "delete"})
	viper.SetDefault(indexColumn, "id, name")
	viper.SetDefault(indexSource, "postgres://pgtoes:secret@127.0.0.1/pgtoes")
	viper.SetDefault(indexTarget, "http://127.0.0.1:9200/")
	viper.SetDefault(logFormat, "text")
	viper.SetDefault(logOutput, "stdout")
	viper.SetDefault(logLevel, "info")
	viper.SetDefault(metricEnabled, true)
	viper.SetDefault(metricPort, 9698)

	// Bind environment variables to keys
	var env map[string]string = make(map[string]string)
	env[indexName] = "INDEX_NAME"
	env[indexFormat] = "INDEX_FORMAT"
	env[indexSeparator] = "INDEX_SEPARATOR"
	env[indexDateformat] = "INDEX_DATEFORMAT"
	env[indexFor] = "INDEX_FOR"
	env[indexWith] = "INDEX_WITH"
	env[indexColumn] = "INDEX_COLUMN"
	env[indexSource] = "INDEX_SOURCE"
	env[indexTarget] = "INDEX_TARGET"
	env[indexTargetURL] = "INDEX_TARGET_URL"
	env[indexTargetCloudID] = "INDEX_TARGET_CLOUDID"
	env[indexTargetUsername] = "INDEX_TARGET_USERNAME"
	env[indexTargetPassword] = "INDEX_TARGET_PASSWORD"
	env[logFormat] = "LOG_FORMAT"
	env[logOutput] = "LOG_OUTPUT"
	env[logLevel] = "LOG_LEVEL"
	env[metricEnabled] = "METRIC_ENABLED"
	env[metricPort] = "METRIC_PORT"

	for k, v := range env {
		_ = viper.BindEnv(k, v)
	}

	cfg := Config{}
	cfg.IndexName = viper.GetString(indexName)
	cfg.IndexFormat = viper.GetBool(indexFormat)
	cfg.IndexSeparator = viper.GetString(indexSeparator)
	cfg.IndexDateFormat = viper.GetString(indexDateformat)
	cfg.IndexFor = viper.GetStringSlice(indexFor)
	cfg.IndexWith = viper.GetStringSlice(indexWith)
	cfg.IndexColumn = viper.GetString(indexColumn)
	cfg.IndexSource = viper.GetString(indexSource)
	cfg.IndexTarget = viper.GetString(indexTarget)
	cfg.IndexTargetURL = viper.GetString(indexTargetURL)
	cfg.IndexTargetCloudID = viper.GetString(indexTargetCloudID)
	cfg.IndexTargetUsername = viper.GetString(indexTargetUsername)
	cfg.IndexTargetPassword = viper.GetString(indexTargetPassword)
	cfg.LogFormat = viper.GetString(logFormat)
	cfg.LogOutput = viper.GetString(logOutput)
	cfg.LogLevel = viper.GetString(logLevel)
	cfg.MetricEnabled = viper.GetBool(metricEnabled)
	cfg.MetricPort = viper.GetInt(metricPort)

	return cfg
}
