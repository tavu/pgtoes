package app

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/cheggaaa/pb/v3"
	"github.com/dustin/go-humanize"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/jackc/pgconn"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tavu/pgtoes/pg"
)

const (
	batchSize int = 255
)

type esBulkResponse struct {
	Errors bool                 `json:"errors"`
	Items  []esBulkResponseItem `json:"items"`
}

type esBulkResponseItem struct {
	Index esBulkResponseItemIndex `json:"index"`
}

type esBulkResponseItemIndex struct {
	ID     string                       `json:"_id"`
	Result string                       `json:"result"`
	Status int                          `json:"status"`
	Error  esBulkResponseItemIndexError `json:"error"`
}

type esBulkResponseItemIndexError struct {
	Type   string                            `json:"type"`
	Reason string                            `json:"reason"`
	Cause  esBulkResponseItemIndexErrorCause `json:"caused_by"`
}

type esBulkResponseItemIndexErrorCause struct {
	Type   string `json:"type"`
	Reason string `json:"reason"`
}

// IndexSnapshotData ...
func (app *App) IndexSnapshotData(snapshotName string) {
	conn := app.GetPgConn()
	start := time.Now().UTC()
	var numErrors, numIndexed int

	// Begin transaction
	{
		sql := "BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;"
		sql += "SET TRANSACTION SNAPSHOT '%s';"
		sql = fmt.Sprintf(sql, snapshotName)
		_, err := conn.Exec(context.Background(), sql).ReadAll()
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}
	for ik := 0; ik < len(app.Config.IndexFor); ik++ {
		tableName := app.Config.IndexFor[ik]
		var (
			tableRow          pg.WALRow
			count, numBatches int
		)

		fmt.Printf("Reading table %q\n", strings.Replace(tableName, "public.", "", 1))
		getTableSchema(conn, tableName, &tableRow)
		fmt.Printf(" - Primary keys: %s\n", strings.Join(tableRow.PrimaryKey.ColumnNames, ", "))

		count = getDocumentCountForTable(conn, tableName)
		fmt.Printf("Found %s documents to be imported\n", humanize.Comma(int64(count)))

		if count%batchSize == 0 {
			numBatches = (count / batchSize)
		} else {
			numBatches = (count / batchSize) + 1
		}
		fmt.Printf(" - Batch size       : %d\n", batchSize)
		fmt.Printf(" - Number of batches: %d\n", numBatches)

		fmt.Println("Importing data.. ")
		pbar := pb.Full.Start(numBatches)
		result := conn.Exec(context.Background(), fmt.Sprintf("SELECT * FROM %s;", tableName))
		for result.NextResult() {
			reader := result.ResultReader()
			defer func() {
				_, err := reader.Close()
				if err != nil {
					fmt.Printf(errStr, err)
				}
			}()
			numIndexed, numErrors = processDBRows(tableName, tableRow, pbar, reader, count, numIndexed, numErrors)
		}
		pbar.Finish()
	}

	// End transaction
	_, err := conn.Exec(context.Background(), "COMMIT;").ReadAll()
	if err != nil {
		fmt.Printf(errStr, err)
	}

	// log.Println(strings.Repeat("▔", 65))
	dur := time.Since(start)
	if numErrors > 0 {
		fmt.Printf(
			"Indexed [%s] documents with [%s] errors in %s (%s docs/sec)\n",
			humanize.Comma(int64(numIndexed)),
			humanize.Comma(int64(numErrors)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(numIndexed))),
		)
	} else {
		fmt.Printf(
			"Successfully indexed [%s] documents in %s (%s docs/sec)\n",
			humanize.Comma(int64(numIndexed)),
			dur.Truncate(time.Millisecond),
			humanize.Comma(int64(1000.0/float64(dur/time.Millisecond)*float64(numIndexed))),
		)
	}
}

func processDBRows(
	tableName string,
	tableRow pg.WALRow,
	pbar *pb.ProgressBar,
	reader *pgconn.ResultReader,
	count,
	numIndexed,
	numErrors int,
) (int, int) {
	var (
		currBatch, numItems, i int
		buf                    bytes.Buffer
	)

	es := app.GetEsClient()

	for reader.NextRow() {
		numItems++
		currBatch = i / batchSize
		if i == count-1 {
			currBatch++
		}
		row := tableRow
		row.SetValuesFromResultReader(reader)
		createdTimestamp := row.ColumnByNameToString("created")
		meta := []byte(
			fmt.Sprintf(
				`{ "index" : { "_id": "%s", "_index" : "%s" } }%s`,
				row.GetID(),
				app.GetIndexName(tableName, createdTimestamp),
				"\n",
			),
		)
		dataObj := gabs.New()
		setJSONData(dataObj, row.ColumnNames, reader.Values())
		data := dataObj.Bytes()
		data = append(data, "\n"...)
		buf.Grow(len(meta) + len(data))
		buf.Write(meta)
		buf.Write(data)

		if i > 0 && i%batchSize == 0 || i == count-1 {
			pbar.Increment()
			res, err := es.Bulk(bytes.NewReader(buf.Bytes()))
			if err != nil {
				fmt.Printf("Failure indexing batch %d: %s\n", currBatch, err)
				os.Exit(1)
			}

			_, numIndexed, numErrors = parseESBulkResponse(res, numItems, numIndexed, numErrors)
			err = res.Body.Close()
			if err != nil {
				fmt.Printf(errStr, err)
			}
			buf.Reset()
			numItems = 0
		}
		i++
	}
	return numIndexed, numErrors
}

func setJSONData(dataObj *gabs.Container, cols []string, vals [][]byte) {
	var (
		t   string
		n   int
		err error
	)
	for i, col := range cols {
		cols[i] = strings.TrimSpace(col)
	}
	for i, col := range cols {
		t = fmt.Sprintf("%s", vals[i])
		if n, err = strconv.Atoi(t); err == nil {
			_, err = dataObj.Set(n, col)
		} else {
			_, err = dataObj.Set(t, col)
		}
		if err != nil {
			fmt.Printf(errStr, err)
		}
	}

}

func parseESBulkResponse(res *esapi.Response, numItems, numIndexed, numErrors int) (int, int, int) {
	var (
		raw map[string]interface{}
		blk *esBulkResponse
	)
	if res.IsError() {
		numErrors += numItems
		if err := json.NewDecoder(res.Body).Decode(&raw); err != nil {
			fmt.Printf("Failure to parse response body: %s", err)
			os.Exit(1)
		} else {
			fmt.Printf("ERROR: [%d] %s: %s",
				res.StatusCode,
				raw["error"].(map[string]interface{})["type"],
				raw["error"].(map[string]interface{})["reason"],
			)
			os.Exit(1)
		}
	} else {
		if err := json.NewDecoder(res.Body).Decode(&blk); err != nil {
			fmt.Printf("Failure to parse response body: %s\n", err)
			os.Exit(1)
		} else {
			numIndexed, numErrors = countStatsOnItems(blk.Items, numIndexed, numErrors)
		}
	}
	return numItems, numIndexed, numErrors
}

func countStatsOnItems(items []esBulkResponseItem, numIndexed int, numErrors int) (int, int) {
	for _, d := range items {
		if d.Index.Status > 201 {
			numErrors++

			fmt.Printf("ERROR: [%d]: %s: %s: %s: %s\n",
				d.Index.Status,
				d.Index.Error.Type,
				d.Index.Error.Reason,
				d.Index.Error.Cause.Type,
				d.Index.Error.Cause.Reason,
			)
		} else {
			numIndexed++
		}
	}
	return numIndexed, numErrors
}

func getTableSchema(conn *pgconn.PgConn, tableName string, tableRow *pg.WALRow) {
	sql := "SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type FROM pg_index i JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey) WHERE i.indrelid = '%s'::regclass AND i.indisprimary;"
	sql = fmt.Sprintf(sql, tableName)
	result2 := conn.Exec(context.Background(), sql)
	for result2.NextResult() {
		reader := result2.ResultReader()
		defer reader.Close()
		for reader.NextRow() {
			tableRow.PrimaryKey.ColumnNames = append(tableRow.PrimaryKey.ColumnNames, string(reader.Values()[0]))
			tableRow.PrimaryKey.ColumnTypes = append(tableRow.PrimaryKey.ColumnTypes, string(reader.Values()[1]))
		}
	}
}

func getDocumentCountForTable(conn *pgconn.PgConn, tableName string) int {
	var count int
	sql := "SELECT COUNT(1) FROM %s;"
	sql = fmt.Sprintf(sql, tableName)
	result2 := conn.Exec(context.Background(), sql)
	for result2.NextResult() {
		reader := result2.ResultReader()
		defer reader.Close()
		for reader.NextRow() {
			count64, err := strconv.ParseInt(string(reader.Values()[0]), 10, 0)
			if err != nil {
				log.Fatalln(err)
			}
			count = int(count64)
		}
	}
	return count
}
