package pg

import (
	"fmt"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/jackc/pgconn"
	log "github.com/sirupsen/logrus"
)

// WALBlob presents data change coming from database
// {"change":[{"kind":"delete","schema":"public","table":"customer","pk":{"pknames":["id"],"pktypes":["integer"]},"oldkeys":{"keynames":["id","name","created","enabled","country"],"keytypes":["integer","character varying","timestamp(0) without time zone","boolean","character varying(2)"],"keyvalues":[11685,"testi 1","2020-10-09 12:56:32",true,"FI"]}},{"kind":"delete","schema":"public","table":"customer","pk":{"pknames":["id"],"pktypes":["integer"]},"oldkeys":{"keynames":["id","name","created","enabled","country"],"keytypes":["integer","character varying","timestamp(0) without time zone","boolean","character varying(2)"],"keyvalues":[11686,"testi 2","2020-10-10 08:19:19",true,"FI"]}}]}
// Example JSON:
// {
// 	"change":
// 	[
// 		{
// 			"kind":"insert",
// 			"schema":"public",
// 			"table":"t",
// 			"columnnames":["id","name"],
// 			"columntypes":["integer","text"],
// 			"columnvalues":[1000,"cat"]
//			"pk":
//			{
//				"pknames":["id"],
//				"pktypes:["integer"]
//			},
//			"oldkeys":
//			{
//				"keynames":["id","name","created","enabled","country"],
//				"keytypes":["integer","character varying","timestamp(0) without time zone","boolean","character varying(2)"],
//				"keyvalues":[11685,"testi 1","2020-10-09 12:56:32",true,"FI"]
//			}
// 		}
// 	]
// }
type WALBlob struct {
	Change []WALRow `json:"change"`
}

// WALRow is invidual change coming from database
type WALRow struct {
	Kind         string           `json:"kind"`
	Schema       string           `json:"schema"`
	Table        string           `json:"table"`
	ColumnNames  []string         `json:"columnnames"`
	ColumnTypes  []string         `json:"columntypes"`
	ColumnValues []interface{}    `json:"columnvalues"`
	PrimaryKey   WALRowPrimaryKey `json:"pk"`
	OldKey       WALRowOldKey     `json:"oldkeys"`
}

// WALRowPrimaryKey has primary key information about the row
type WALRowPrimaryKey struct {
	ColumnNames []string `json:"pknames"`
	ColumnTypes []string `json:"pktypes"`
}

// WALRowOldKey has old primary key information about the row
type WALRowOldKey struct {
	KeyNames  []string      `json:"keynames"`
	KeyTypes  []string      `json:"keytypes"`
	KeyValues []interface{} `json:"keyvalues"`
}

// GetID returns computed ID value for WALRow
func (row WALRow) GetID() string {
	var tmp []string
	for i := 0; i < len(row.PrimaryKey.ColumnNames); i++ {
		pkName := row.PrimaryKey.ColumnNames[i]
		for j := 0; j < len(row.ColumnNames); j++ {
			colName := row.ColumnNames[j]
			if pkName == colName {
				tmp = append(tmp, row.ColumnByPosToString(j))
			}
		}
	}
	return strings.Join(tmp, "-")
}

// GetOldID returns computed previous ID value for WALRow
func (row WALRow) GetOldID() string {
	var tmp []string
	for i := 0; i < len(row.PrimaryKey.ColumnNames); i++ {
		pkName := row.PrimaryKey.ColumnNames[i]
		for j := 0; j < len(row.OldKey.KeyNames); j++ {
			colName := row.OldKey.KeyNames[j]
			if pkName == colName {
				var val string
				switch row.OldKey.KeyTypes[j] {
				case "integer":
					val = fmt.Sprint(int(row.OldKey.KeyValues[j].(float64)))
				default:
					val = fmt.Sprint(row.OldKey.KeyValues[j])
				}
				tmp = append(tmp, val)
			}
		}
	}
	return strings.Join(tmp, "-")
}

// ToJSON returns normalized version of WALRow as JSON
func (row WALRow) ToJSON() string {
	json := gabs.New()
	for k, v := range row.ColumnNames {
		var err error
		switch row.ColumnValues[k].(type) {
		case nil:
			_, err = json.Set("", v)
		case bool:
			if row.ColumnValues[k].(bool) {
				_, err = json.Set("t", v)
			} else {
				_, err = json.Set("f", v)
			}
		default:
			_, err = json.Set(row.ColumnValues[k], v)
		}
		if err != nil {
			log.Warnln(err)
		}
	}
	return json.String()
}

// ColumnByPosToString returns column value by position as string
func (row WALRow) ColumnByPosToString(j int) string {
	val := fmt.Sprint(row.ColumnValues[j])
	if len(row.ColumnTypes) != 0 {
		switch row.ColumnTypes[j] {
		case "integer":
			switch row.ColumnValues[j].(type) {
			case float64:
				val = fmt.Sprint(int(row.ColumnValues[j].(float64)))
			case []byte:
				val = string(row.ColumnValues[j].([]byte)[:])
			}
		}
	}
	return val
}

// ColumnByNameToString returns column value by name as string
func (row WALRow) ColumnByNameToString(name string) string {
	for i := 0; i < len(row.ColumnNames); i++ {
		if row.ColumnNames[i] == name {
			return fmt.Sprint(row.ColumnValues[i])
		}
	}
	return ""
}

// SetValuesFromResultReader overrides row values with values coming from result reader
func (row *WALRow) SetValuesFromResultReader(reader *pgconn.ResultReader) {
	// Assign column names and values to copy of tableRow
	desc := reader.FieldDescriptions()
	for fd := 0; fd < len(desc); fd++ {
		row.ColumnNames = append(row.ColumnNames, string(desc[fd].Name))
	}
	vals := reader.Values()
	for vk := 0; vk < len(vals); vk++ {
		row.ColumnValues = append(row.ColumnValues, string(vals[vk]))
	}
}
