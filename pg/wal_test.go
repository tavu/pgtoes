package pg_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/Jeffail/gabs/v2"
	"github.com/jackc/pgconn"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tavu/pgtoes/pg"
)

var walRowIDTests = []struct {
	in  []byte
	out string
}{
	{
		[]byte(`{"kind":"insert","schema":"public","table":"grant_access","columnnames":["id","account","service","granted"],"columntypes":["integer","integer","integer","timestamp(0) without time zone"],"columnvalues":[6912239,22832,12220,"2020-10-09 14:55:03"],"pk":{"pknames":["id"],"pktypes":["integer"]}}`),
		"6912239",
	},
	{
		[]byte(`{"kind":"insert","schema":"public","table":"foo_bar_link","columnnames":["foo","bar", "a", "b"],"columntypes":["integer","integer","text","text"],"columnvalues":[123, 456, "tadaa", "puup"],"pk":{"pknames":["foo", "bar"],"pktypes":["integer", "integer"]}}`),
		"123-456",
	},
	{
		[]byte(`{"kind":"insert","schema":"public","table":"foo_bar_link","columnnames":["foo","bar", "a", "b"],"columntypes":[],"columnvalues":[222, 333, "tadaa", "puup"],"pk":{"pknames":["foo", "bar"],"pktypes":["integer", "integer"]}}`),
		"222-333",
	},
}

var walRowOldIDTests = []struct {
	in  []byte
	out string
}{
	{
		[]byte(`{"kind":"delete","schema":"public","table":"customer","pk":{"pknames":["id"],"pktypes":["integer"]},"oldkeys":{"keynames":["id","name","created","enabled","country"],"keytypes":["integer","character varying","timestamp(0) without time zone","boolean","character varying(2)"],"keyvalues":[11685,"testi 1","2020-10-09 12:56:32",true,"FI"]}}`),
		"11685",
	},
}

func TestGetID(t *testing.T) {
	for _, tt := range walRowIDTests {
		var row pg.WALRow
		if err := json.Unmarshal(tt.in, &row); err != nil {
			fmt.Println(err)
		}
		s := row.GetID()
		if s != tt.out {
			t.Errorf("got %q, want %q", s, tt.out)
		}
	}
}

func TestGetOldID(t *testing.T) {
	for _, tt := range walRowOldIDTests {
		var row pg.WALRow
		if err := json.Unmarshal(tt.in, &row); err != nil {
			fmt.Println(err)
		}
		s := row.GetOldID()
		if s != tt.out {
			t.Errorf("got %q, want %q", s, tt.out)
		}
	}
}

func TestGetIDFormatting(t *testing.T) {
	foo := []byte(`{"kind":"insert","schema":"public","table":"grant_access","columnnames":["id","account","service","granted"],"columntypes":["integer","integer","integer","timestamp(0) without time zone"],"columnvalues":[6912239,22832,12220,"2020-10-09 14:55:03"],"pk":{"pknames":["id"],"pktypes":["integer"]}}`)
	var row pg.WALRow
	if err := json.Unmarshal(foo, &row); err != nil {
		fmt.Println(err)
	}

	// Values as []byte
	row.ColumnValues[0] = []byte{51, 55, 52, 54, 56}
	s := row.GetID()
	out := "37468"
	if s != out {
		t.Errorf("got %q, want %q", s, out)
	}

}

func TestJSONFormatting(t *testing.T) {
	{
		foo := []byte(`{"kind":"insert","schema":"public","table":"grant_access","columnnames":["id","account","service","granted"],"columntypes":["integer","integer","integer","timestamp(0) without time zone"],"columnvalues":[6912239,22832,12220,"2020-10-09 14:55:03"],"pk":{"pknames":["id"],"pktypes":["integer"]}}`)
		var row pg.WALRow
		if err := json.Unmarshal(foo, &row); err != nil {
			fmt.Println(err)
		}
		jsonData := []byte(row.ToJSON())
		jsonParsed, err := gabs.ParseJSON(jsonData)
		if err != nil {
			fmt.Println(err)
		}
		var value float64
		value, _ = jsonParsed.Path("account").Data().(float64)
		assert.Equal(t, float64(22832), value, "account integer")

		value, _ = jsonParsed.Path("service").Data().(float64)
		assert.Equal(t, float64(12220), value, "service integer")
	}
	{
		foo := []byte(`{"kind":"update","schema":"public","table":"account","columnnames":["id","email","ldap","name","customer","password","created","language","openid_provider","enabled","services_updated","deleted","pw_reset_token","failed_logins","locked","country"],"columntypes":["integer","character varying","integer","character varying","integer","character varying","timestamp(0) without time zone","character varying(5)","integer","boolean","timestamp without time zone","timestamp without time zone","character varying","integer","boolean","character varying(2)"],"columnvalues":[32399,"donotchange17@hakumediat.fi",null,null,4,"{\"alg\":\"sha512\",\"hmac\":true,\"salt\":\"7fbd42b8dfa14da8507769a28d21c59a\",\"saltyside\":null,\"hash\":\"b3d8891ad8e847a1d489efc1108a81a4ed998976bbdba2767b5f7daf53e87fe0939b025e97eb4edfacfbff12b5e9d0d44c7e2d711db85dd3ab3e8b79952a36d9\"}","2019-05-20 14:35:26","FI",null,true,"2019-05-20 14:35:26",null,null,0,null,null],"pk":{"pknames":["id"],"pktypes":["integer"]},"oldkeys":{"keynames":["id"],"keytypes":["integer"],"keyvalues":[32399]}}`)
		var row pg.WALRow
		if err := json.Unmarshal(foo, &row); err != nil {
			fmt.Println(err)
		}
		jsonData := []byte(row.ToJSON())
		jsonParsed, err := gabs.ParseJSON(jsonData)
		if err != nil {
			fmt.Println(err)
		}
		var value string
		value, _ = jsonParsed.Path("ldap").Data().(string)
		assert.Equal(t, "", value, "ldap null to string")

		value, _ = jsonParsed.Path("enabled").Data().(string)
		assert.Equal(t, "t", value, "enabled true to string")
	}
}

func TestColumnByNameToString(t *testing.T) {
	{
		foo := []byte(`{"kind":"insert","schema":"public","table":"grant_access","columnnames":["id","account","service","granted"],"columntypes":["integer","integer","integer","timestamp(0) without time zone"],"columnvalues":[6912239,22832,12220,"2020-10-09 14:55:03"],"pk":{"pknames":["id"],"pktypes":["integer"]}}`)
		var row pg.WALRow
		if err := json.Unmarshal(foo, &row); err != nil {
			fmt.Println(err)
		}
		assert.Equal(t, "22832", row.ColumnByNameToString("account"))
	}
}

func TestSetValuesFromResultReader(t *testing.T) {
	{
		foo := []byte(`{"kind":"insert","schema":"public","table":"grant_access","columnnames":["id","account","service","granted"],"columntypes":["integer","integer","integer","timestamp(0) without time zone"],"columnvalues":[6912239,22832,12220,"2020-10-09 14:55:03"],"pk":{"pknames":["id"],"pktypes":["integer"]}}`)
		var row pg.WALRow
		if err := json.Unmarshal(foo, &row); err != nil {
			fmt.Println(err)
		}
		reader := pgconn.ResultReader{}
		row.SetValuesFromResultReader(&reader)
	}
}
