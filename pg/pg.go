package pg

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jackc/pgconn"
	"github.com/jackc/pglogrepl"
	"github.com/jackc/pgproto3/v2"
	log "github.com/sirupsen/logrus"
)

// FnProcessData function type for processing WAL entries
type FnProcessData func(string, *WALBlob)

// DropReplicationSlotIfExists drops replication connection if it exists
func DropReplicationSlotIfExists(ctx context.Context, conn *pgconn.PgConn, pubName string) error {
	results, err := conn.Exec(context.Background(), fmt.Sprintf("SELECT slot_name FROM pg_replication_slots WHERE slot_name='%s';", pubName)).ReadAll()
	if err != nil {
		return err
	}
	if len(results[0].Rows) != 0 {
		return pglogrepl.DropReplicationSlot(ctx, conn, pubName, pglogrepl.DropReplicationSlotOptions{})
	}
	return nil
}

// DropPublicationIfExists drops publication if it exists
func DropPublicationIfExists(ctx context.Context, conn *pgconn.PgConn, pubName string) error {
	_, err := conn.Exec(ctx, fmt.Sprintf("DROP PUBLICATION IF EXISTS %s", pubName)).ReadAll()
	return err
}

// CreatePublicationIfNotExists creates publication on database if not already exists
func CreatePublicationIfNotExists(ctx context.Context, conn *pgconn.PgConn, pubName string, pubFor string, pubWith string) error {
	results, err := conn.Exec(ctx, fmt.Sprintf("SELECT COUNT(1) FROM pg_catalog.pg_publication WHERE pubname = '%s'", pubName)).ReadAll()
	if err != nil {
		return err
	}
	if "0" == string(results[0].Rows[0][0]) {
		_, err = conn.Exec(ctx, fmt.Sprintf("CREATE PUBLICATION %s FOR TABLE %s WITH (publish = '%s')", pubName, pubFor, pubWith)).ReadAll()
		if err != nil {
			return err
		}
	}
	return nil
}

// CreateReplicationSlotIfNotExists creates replication slot on database if not already exists
func CreateReplicationSlotIfNotExists(ctx context.Context, conn *pgconn.PgConn, slotName string) (pglogrepl.CreateReplicationSlotResult, error) {
	var slotResult pglogrepl.CreateReplicationSlotResult
	results, err := conn.Exec(ctx, fmt.Sprintf("SELECT * FROM pg_replication_slots WHERE slot_name = '%s'", slotName)).ReadAll()
	if err != nil {
		return slotResult, err
	}
	if 0 == len(results[0].Rows) {
		slotResult, err = pglogrepl.CreateReplicationSlot(ctx, conn, slotName, "wal2json", pglogrepl.CreateReplicationSlotOptions{}) // default action is EXPORT_SNAPSHOT
		if err != nil {
			return slotResult, err
		}
	} else {
		// See IDENTIFY_SYSTEM on https://www.postgresql.org/docs/11/protocol-replication.html
		sysident, err := pglogrepl.IdentifySystem(ctx, conn)
		if err != nil {
			return slotResult, err
		}
		slotResult.SlotName = slotName
		slotResult.ConsistentPoint = sysident.XLogPos.String()
		slotResult.OutputPlugin = string(results[0].Rows[0][1])
	}
	return slotResult, nil
}

// StartReplication starts listening WAL events from database
func StartReplication(ctx context.Context, conn *pgconn.PgConn, slotName string, lsn string, processor *[]*FnProcessData, config []string) error {
	startLSN, err := pglogrepl.ParseLSN(lsn)
	if err != nil {
		return err
	}

	var pluginArguments []string
	pluginArguments = []string{"\"include-pk\" '1'", "\"format-version\" '1'"}
	pluginArguments = append(pluginArguments, config...)

	opts := pglogrepl.StartReplicationOptions{PluginArgs: pluginArguments}
	if err := pglogrepl.StartReplication(context.Background(), conn, slotName, startLSN, opts); err != nil {
		return err
	}

	clientXLogPos := startLSN
	standbyMessageTimeout := time.Second * 10
	nextStandbyMessageDeadline := time.Now().Add(standbyMessageTimeout)

	for {
		if time.Now().After(nextStandbyMessageDeadline) {
			err = pglogrepl.SendStandbyStatusUpdate(context.Background(), conn, pglogrepl.StandbyStatusUpdate{WALWritePosition: clientXLogPos})
			if err != nil {
				return fmt.Errorf("SendStandbyStatusUpdate: %s", err)
			}
			nextStandbyMessageDeadline = time.Now().Add(standbyMessageTimeout)
		}

		ctx, cancel := context.WithDeadline(context.Background(), nextStandbyMessageDeadline)
		msg, err := conn.ReceiveMessage(ctx)
		cancel()
		if err != nil {
			if pgconn.Timeout(err) {
				continue
			}
			return fmt.Errorf("ReceiveMessage: %s", err)
		}

		if nextStandbyMessageDeadline, clientXLogPos, err = processWALMessage(nextStandbyMessageDeadline, clientXLogPos, msg, processor); err != nil {
			return err
		}
	}
}

func processWALMessage(nextStandbyMessageDeadline time.Time, clientXLogPos pglogrepl.LSN, msg pgproto3.BackendMessage, processor *[]*FnProcessData) (time.Time, pglogrepl.LSN, error) {
	switch msg := msg.(type) {
	case *pgproto3.NoticeResponse:
		log.Debugf("msg.(pgproto3.NoticeResponse)=%+v", msg)
	case *pgproto3.CopyData:
		switch msg.Data[0] {
		case pglogrepl.PrimaryKeepaliveMessageByteID:
			pkm, err := pglogrepl.ParsePrimaryKeepaliveMessage(msg.Data[1:])
			if err != nil {
				return nextStandbyMessageDeadline, clientXLogPos, fmt.Errorf("ParsePrimaryKeepaliveMessage: %s", err)
			}
			if pkm.ReplyRequested {
				nextStandbyMessageDeadline = time.Time{}
			}
		case pglogrepl.XLogDataByteID:
			xld, err := pglogrepl.ParseXLogData(msg.Data[1:])
			if err != nil {
				return nextStandbyMessageDeadline, clientXLogPos, fmt.Errorf("ParseXLogData: %s", err)
			}
			clientXLogPos = xld.WALStart + pglogrepl.LSN(len(xld.WALData))
			var blob WALBlob
			log.Debugf("xld.WALData=%+v", string(xld.WALData))
			if err := json.Unmarshal(xld.WALData, &blob); err != nil {
				return nextStandbyMessageDeadline, clientXLogPos, fmt.Errorf("Error Decoding: %s", err)
			}
			processWALBlob(processor, clientXLogPos, &blob)
		}
	default:
		return nextStandbyMessageDeadline, clientXLogPos, fmt.Errorf("Received unexpected message: %#v", msg)
	}
	return nextStandbyMessageDeadline, clientXLogPos, nil
}

func processWALBlob(processor *[]*FnProcessData, lsn pglogrepl.LSN, blob *WALBlob) {
	for _, p := range *processor {
		var fn FnProcessData = *p
		fn(lsn.String(), blob)
	}
}
