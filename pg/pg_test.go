package pg_test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/jackc/pgconn"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tavu/pgtoes/pg"
)

var myPgConn *pgconn.PgConn
var myReplConn *pgconn.PgConn

func GetPgConn() *pgconn.PgConn {
	if myPgConn == nil {
		conn, err := pgconn.Connect(context.Background(), os.Getenv("INDEX_SOURCE"))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		myPgConn = conn
	}
	return myPgConn
}

func GetReplConn() *pgconn.PgConn {
	if myReplConn == nil {
		conn, err := pgconn.Connect(context.Background(), fmt.Sprintf("%s?replication=database", os.Getenv("INDEX_SOURCE")))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		myReplConn = conn
	}
	return myReplConn
}

func TestCreatePublicationIfNotExists(t *testing.T) {
	replConn := GetReplConn()
	{
		err := pg.DropPublicationIfExists(context.Background(), replConn, "pgtoes")
		if err != nil {
			t.Error(err)
		}
	}

	{
		err := pg.CreatePublicationIfNotExists(context.Background(), replConn, "pgtoes", "t", "insert, update, delete")
		if err != nil {
			t.Error(err)
		}
		pgConn := GetPgConn()
		result := pgConn.ExecParams(context.Background(), "SELECT * FROM pg_publication WHERE pubname=$1", [][]byte{[]byte("pgtoes")}, nil, nil, nil)
		if !result.NextRow() {
			t.Error("Failed to create publication")
		}
		result.Close()
	}
}

func TestCreateReplicationSlotIfNotExists(t *testing.T) {
	replConn := GetReplConn()
	err := pg.DropReplicationSlotIfExists(context.Background(), replConn, "pgtoes")
	if err != nil {
		t.Error(err)
	}

	myres1, err := pg.CreateReplicationSlotIfNotExists(context.Background(), replConn, "pgtoes")
	if err != nil {
		t.Error(err)
	}
	pgConn := GetPgConn()
	result := pgConn.ExecParams(context.Background(), "SELECT * FROM pg_replication_slots WHERE slot_name=$1", [][]byte{[]byte("pgtoes")}, nil, nil, nil)
	if !result.NextRow() {
		t.Error("Failed to create replication slot")
	}
	result.Close()
	myres2, err := pg.CreateReplicationSlotIfNotExists(context.Background(), replConn, "pgtoes")
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, myres1.ConsistentPoint, myres2.ConsistentPoint, "preload replication slot - consistentpoint")
	assert.Equal(t, myres1.OutputPlugin, myres2.OutputPlugin, "preload replication slot - outputplugin")
	assert.Equal(t, myres1.SlotName, myres2.SlotName, "preload replication slot - slotname")
	assert.Equal(t, "", myres2.SnapshotName, "snapshot is only created on first creation time")
}

func TestDropPublicationIfExists(t *testing.T) {
	pgConn := GetPgConn()

	// check that publication is already there
	{
		results, err := pgConn.Exec(context.Background(), "SELECT pubname FROM pg_publication WHERE pubname='pgtoes';").ReadAll()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, "pgtoes", string(results[0].Rows[0][0]), "pg_publication.pubname = pgtoes is found")
	}

	// drop publication
	replConn := GetReplConn()
	err := pg.DropPublicationIfExists(context.Background(), replConn, "pgtoes")
	if err != nil {
		t.Error(err)
	}

	// verify that publication was really removed
	{
		results, err := pgConn.Exec(context.Background(), "SELECT pubname FROM pg_publication WHERE pubname='pgtoes';").ReadAll()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, 0, len(results[0].Rows), "pg_publication.pubname = pgtoes is not found")
	}
}

func TestDropReplicationIfExists(t *testing.T) {
	pgConn := GetPgConn()

	// check that publication is already there
	{
		results, err := pgConn.Exec(context.Background(), "SELECT slot_name FROM pg_replication_slots WHERE slot_name='pgtoes';").ReadAll()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, "pgtoes", string(results[0].Rows[0][0]), "pg_replication_slots.slot_name = pgtoes is found")
	}

	// drop replication
	replConn := GetReplConn()
	err := pg.DropReplicationSlotIfExists(context.Background(), replConn, "pgtoes")
	if err != nil {
		t.Error(err)
	}

	// verify that publication was really removed
	{
		results, err := pgConn.Exec(context.Background(), "SELECT slot_name FROM pg_replication_slots WHERE slot_name='pgtoes';").ReadAll()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, 0, len(results[0].Rows), "pg_replication_slots.slot_name = pgtoes is not found")
	}

}

func TestStartReplication(t *testing.T) {
	replConn := GetReplConn()
	pgConn := GetPgConn()
	ctx := context.Background()
	slotName := "testpgtoes2"
	_ = pg.CreatePublicationIfNotExists(ctx, replConn, slotName, "t", "insert")
	slot, err := pg.CreateReplicationSlotIfNotExists(ctx, replConn, slotName)
	if err != nil {
		t.Error(err)
	}

	{
		done := make(chan bool)
		var blob *pg.WALBlob
		go func() {
			var fn pg.FnProcessData = func(_ string, b *pg.WALBlob) {
				blob = b
				done <- true
			}
			var pd []*pg.FnProcessData
			pd = append(pd, &fn)
			var config []string
			_ = pg.StartReplication(ctx, replConn, slot.SlotName, slot.ConsistentPoint, &pd, config)
		}()
		go func() {
			_, err := pgConn.Exec(context.Background(), "INSERT INTO t VALUES (8, 'eightball');").ReadAll()
			if err != nil {
				t.Error(err)
			}
		}()
		<-done
		assert.Equal(t, "insert", blob.Change[0].Kind, "kind is insert")
		assert.Equal(t, "public", blob.Change[0].Schema, "schema is public")
		assert.Equal(t, "t", blob.Change[0].Table, "table is t")
		assert.Equal(t, "id", blob.Change[0].ColumnNames[0], "column name is id")
		assert.Equal(t, "name", blob.Change[0].ColumnNames[1], "column name is name")
		assert.Equal(t, "integer", blob.Change[0].ColumnTypes[0], "id type is integer")
		assert.Equal(t, "text", blob.Change[0].ColumnTypes[1], "name type is text")
		assert.Equal(t, 8, int(blob.Change[0].ColumnValues[0].(float64)), "id is 8")
		assert.Equal(t, "eightball", string(blob.Change[0].ColumnValues[1].(string)), "name is eigthball")
	}
}
