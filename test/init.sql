CREATE ROLE pgtoes WITH
    SUPERUSER -- must be superuser to create FOR ALL TABLES publication
    LOGIN 
    REPLICATION 
    PASSWORD 'secret'
    ;
CREATE DATABASE pgtoes;
ALTER DATABASE pgtoes OWNER TO pgtoes;
\c pgtoes
CREATE TABLE t (id int, name TEXT, PRIMARY KEY(id));
INSERT INTO t VALUES (1, 'foo');
INSERT INTO t VALUES (2, 'bar');
